import nltk
from bs4 import BeautifulSoup
from urllib.request import urlopen
from nltk.tokenize import word_tokenize
from nltk.collections import Counter

# extract all the contents of the text file.
#path = "https://www.nytimes.com/2021/03/02/opinion/misinformation-conspiracy-theories.html?action=click&module=Opinion&pgtype=Homepage"
path = "https://edition.cnn.com/europe/live-news/ukraine-russia-putin-news-03-04-22/h_a0de992b8ad74327a8ce8ee714acc995"
path = "https://www.clarin.gr/"
path = "https://www.in.gr/2022/05/26/go-fun/music-stage/mad-vma-poia-itan-proti-fora-tis-elenis-foureira-ston-mousiko-thesmo/"

raw = urlopen(path).read()

print('raw:', raw)

# remove html/xml tags
clean = BeautifulSoup(raw, features="html.parser")
# get text and print it
text= clean.get_text()
print('clean:', text)

# tokenize as usual
tokens = nltk.word_tokenize(text)
# count
counts = Counter(tokens)
print(counts)
